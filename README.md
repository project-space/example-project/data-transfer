# Example Data Transfer

In this example, two different data transfers are depicted:
- Data transfer 1: A single data transfer concerning only SPHN concepts based on the official SPHN RDF Schema 2023.2
- Data transfer 2: Multiple data transfers with a specific frequency and timeline based on a project RDF Schema (version-1)
